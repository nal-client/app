package sample;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.jfoenix.controls.JFXDatePicker;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.concurrent.CountDownLatch;

public class ReminderController implements Initializable {
    private Parent parent;
    private Stage stage;
    private Scene scene;

    @FXML
    private TextField txtTitle;
    @FXML
    private TextArea txtMessage;
    @FXML
    private ComboBox<String> cboNotificationType;
    @FXML
    private ComboBox<String> cboAnimations;
    @FXML
    private ComboBox<Integer> delayTime;
    @FXML
    private TextField dateSelected;
    @FXML
    private TextField timeSelected;
    @FXML
    private Button btnSave;


    String folder = System.getProperty("user.home")+File.separator+"NAL_Calendar";
    String path=System.getProperty("user.home")+"\\photo_log.txt";
    String data="";

    public ReminderController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("reminder.fxml"));
        fxmlLoader.setController(this);
        try {
            parent = (Parent) fxmlLoader.load();
            scene = new Scene(parent,300,500);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void launchController(Stage stage) {
        this.stage = stage;
        stage.setTitle("New Reminder");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.hide();
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.btnSave.setTooltip(new Tooltip("Save Reminder"));
        this.cboNotificationType.getItems().addAll(new String[] { "Success", "Information", "Notice", "Warning", "Error"});
        this.cboAnimations.getItems().addAll(new String[] { "Slide", "Fade", "Popup" });
        this.delayTime.getItems().addAll(new Integer[] { 5,10,15,20,25,30,35,40,45,50,55,60 });
        this.delayTime.getSelectionModel().selectLast();
        this.cboNotificationType.getSelectionModel().selectFirst();
        this.cboAnimations.getSelectionModel().selectFirst();
        this.cboAnimations.setTooltip(new Tooltip("Select Animation type for notification"));
        this.cboNotificationType.setTooltip(new Tooltip("Select notification outlook"));
        this.txtTitle.setTooltip(new Tooltip("Enter title for notification"));
        this.txtMessage.setTooltip(new Tooltip("Enter body of message for notification"));
        dateSelected.setText(LocalDate.now().toString());
        timeSelected.setText(LocalTime.now().toString());
        dateSelected.setTooltip(new Tooltip("Select date to be notified on"));
        timeSelected.setTooltip(new Tooltip("Select time to be notified on"));

        checkFileExist();

        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String title=txtTitle.getText();
                String message=txtMessage.getText();
                String animationType=cboAnimations.getValue();
                String notificationType=cboNotificationType.getValue();
                String date= dateSelected.getText().toString();
                String time= timeSelected.getText().toString();
                int delay= delayTime.getValue();

                if(title.length()<1) txtTitle.requestFocus();
                else if(message.length()<1) txtMessage.requestFocus();
                else {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("title", title);
                        jsonObject.put("message", message);
                        jsonObject.put("animationType", animationType);
                        jsonObject.put("notificationType", notificationType);
                        jsonObject.put("date",date);
                        jsonObject.put("time",time);
                        jsonObject.put("delay", delay);
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                    String jsonString = jsonObject.toString();
                    System.out.println(jsonString);

                    saveUpcomingMeeting(dateSelected.getText().toString(),jsonString);
                }
            }
        });
    }

    private void saveUpcomingMeeting(String date, String data){
        File file = new File(folder);
        if(!file.exists()){
            file.mkdir();
        }
        try {
            String filename = folder+File.separator+"upcomingmeeting_"+date+".txt";
            System.out.println(filename);
            PrintWriter printWriter = new PrintWriter(new FileWriter(filename,true));
            printWriter.println(data);
            printWriter.close();

            Controller controller = new Controller();
            controller.launchController(stage);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void showMessage(String msg) {
        Dialog<String> dialog = new Dialog<String>();
        dialog.setTitle("Message");
        ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        dialog.setContentText(msg);
        dialog.getDialogPane().getButtonTypes().add(type);
        dialog.showAndWait();
    }

    private void checkFileExist() {
        File f=new File(path);
        if(!f.exists()){
            try {
                FileOutputStream fos=new FileOutputStream(path);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
