package sample;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.skins.JFXDatePickerSkin;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;
import sun.java2d.pipe.SpanShapeRenderer;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CountDownLatch;

public class Controller implements Initializable, MqttCallback {
    private static final String MQTT_URL = "tcp://broker.hivemq.com:1883";
    private static final String MQTT_TOPIC_GENERAL = "nal_general";
    private static final String MQTT_TOPIC_IMG = "nal_img";
    private static final String MQTT_TOPIC_HOLIDAY = "nal_holiday";

    private Stage stage;
    private Parent parent;
    private Scene scene;

    @FXML
    VBox cv;
    @FXML
    ListView upcomingListView;
    @FXML
    ListView generalListView;
    @FXML
    ListView todaysSpecialListView;
    @FXML
    Button setReminderButton;
    @FXML
    ImageView imageView_header;

    String folder = System.getProperty("user.home")+File.separator+"NAL_Calendar";
    String filepathtext = System.getProperty("user.home")+File.separator+"NAL_Calendar\\nal_log.txt";
    String img_filepathtext = System.getProperty("user.home")+File.separator+"NAL_Calendar\\nal_img.txt";
    String path = "";
    String filepath = "";
    String img_filepath = "";
    String data = "";
    String DOWNLOAD_URL = "http://localhost:8080/nalserver/Download";

    Hashtable<String,Holiday> holidayHashtable;
    ArrayList<String> todaySpecialList;
    ArrayList<General> generalList;
    ArrayList<Upcoming> upcomingList;

    TrayNotification tray;

    public Controller(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample.fxml"));
        fxmlLoader.setController(this);
        URL location = Controller.class.getProtectionDomain().getCodeSource().getLocation();
        path = location.getFile()+"/sample";
        //System.out.println("aaaaaaaaa"+location.getFile());
        try {
            parent = (Parent) fxmlLoader.load();
            scene = new Scene(parent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void launchController(Stage stage) {
        this.stage = stage;
        stage.setTitle("User Home");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setHeight(600);
        stage.hide();
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        holidayHashtable = new Hashtable<>();
        todaySpecialList = new ArrayList<>();
        upcomingList = new ArrayList<>();
        generalList = new ArrayList<>();

        checkFolder();
        checkImgFile();
        checkHolidayFile();
        if(filepath!=null && !filepath.equals("")){
            //System.out.println("Current filepath "+filepath);
            readExcel(filepath);
        }

        if(img_filepath!=null && !img_filepath.equals("")){
            System.out.println("Current filepath "+img_filepath);
            Image i = null;
            try {
                i = new Image(new FileInputStream(img_filepath));
                if(imageView_header!=null)
                imageView_header.setImage(i);
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            }

        }
        try {
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }



        this.tray = new TrayNotification();
        //populateCalendar();
        LocalDate localDate = LocalDate.now();
        checkFolder();
        getTodaysSpecial(localDate);
        getGeneralMeetings(localDate);
        getUpcomingMeetings(localDate);

        setReminderButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ReminderController reminderController = new ReminderController();
                reminderController.launchController(stage);
            }
        });
    }

    private void readExcel(String filePath) {
        try
        {
            //System.out.println("readExcel : "+filePath);
            FileInputStream file = new FileInputStream(new File(filePath));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<org.apache.poi.ss.usermodel.Cell> cellIterator = row.cellIterator();
                String names[] = new String[3];
                int i=0;
                while (cellIterator.hasNext())
                {
                    org.apache.poi.ss.usermodel.Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly

                    switch (cell.getCellType())
                    {
                        case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t");
                            break;
                        case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "\t");
                            names[i] = cell.getStringCellValue();
                            i++;
                            break;
                    }

                }
                Holiday holiday = new Holiday(names[1],names[2]);
                Date d = null;
                try {
                    d = new SimpleDateFormat("dd\\MM\\yyyy").parse(names[0]);
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(d);
                    holidayHashtable.put(date,holiday);
                } catch (ParseException e) {
                    System.out.println("Error while parsing");
                    e.printStackTrace();
                }

                System.out.println("");

            }
            file.close();
            System.out.println("holidays : "+holidayHashtable);
            populateCalendar();
        }
        catch (IOException e)
        {
            System.out.println("Exception While ReadExcel");
            e.printStackTrace();
        }
    }

    private void populateCalendar(){
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);

                        if(item.getDayOfWeek().equals(DayOfWeek.SUNDAY) || item.getDayOfWeek().equals(DayOfWeek.SATURDAY))
                        {
                            System.out.println(item.getDayOfWeek());
                            //setTooltip(new Tooltip("Beware the Ides of March!"));
                            setStyle("-fx-text-fill: red;");
                        }
                        if(holidayHashtable.containsKey(item.toString())){
                            String holidayName = holidayHashtable.get(item.toString()).getHolidayName();
                            String holidayType = holidayHashtable.get(item.toString()).getHolidayType();

                            setTooltip(new Tooltip(holidayName));
                            if(holidayType.equalsIgnoreCase("General")){
                                setStyle("-fx-text-fill: red;");
                            }
                            else{
                                setStyle("-fx-text-fill:blue;");
                            }
                        }
                    }
                };
            }
        };
        JFXDatePicker picker = new JFXDatePicker(LocalDate.now());
        picker.setDayCellFactory(dayCellFactory);
        picker.valueProperty().addListener(new ChangeListener<LocalDate>() {
            @Override
            public void changed(ObservableValue<? extends LocalDate> observable, LocalDate oldValue, LocalDate newValue) {
                getTodaysSpecial(newValue);
                getGeneralMeetings(newValue);
                getUpcomingMeetings(newValue);
            }
        });


        JFXDatePickerSkin d=new JFXDatePickerSkin(picker);

        Node n = d.getPopupContent();
        n.prefHeight(100);
        this.cv.getChildren().addAll(n);
    }

    private void checkFolder(){
        File file = new File(folder);
        if(!file.exists()){
            file.mkdir();
        }
    }

    private void checkHolidayFile(){
        File file = new File(filepathtext);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                filepath = br.readLine();
                br.close();
                System.out.println("checkHolidayFile "+filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }private void checkImgFile(){
        File file = new File(img_filepathtext);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                img_filepath = br.readLine();
                br.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void getTodaysSpecial(LocalDate localDate){
        todaySpecialList.clear();
        todaysSpecialListView.getItems().clear();
        String date = localDate.toString();
        System.out.println("getTodaysSpecial "+date);
        if(holidayHashtable.containsKey(date)){
            todaySpecialList.add(holidayHashtable.get(date).toString());
        }
        else{
            todaySpecialList.add("No Special Event Today");
        }
        System.out.println("getTodaysSpecial list "+todaySpecialList);
        todaysSpecialListView.getItems().addAll(todaySpecialList);
    }

    private void getGeneralMeetings(LocalDate localDate){
        generalList.clear();
        generalListView.getItems().clear();
        String date = localDate.toString();
        System.out.println("getGeneralMeetings "+date);
        File file = new File(folder+File.separator+"generalmeeting_"+date+".txt");
        if(file.exists()){
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String data = "";
                try{
                    while((data = bufferedReader.readLine())!=null){

                        buildGeneralNotification(data);
                    }
                    bufferedReader.close();

                }catch (IOException e){
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (generalList.size()==0){
            generalList.add(new General("No General Meeting Today"));
        }
        generalListView.getItems().addAll(generalList);
        System.out.println("getGeneralMeetings list "+generalList);
    }

    private void getUpcomingMeetings(LocalDate localDate){
        upcomingList.clear();
        upcomingListView.getItems().clear();
        String date = localDate.toString();
        System.out.println("getUpcomingMeetings "+date);
        File file = new File(folder+File.separator+"upcomingmeeting_"+date+".txt");

        if(file.exists()){
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String data = "";
                try{
                    while((data = bufferedReader.readLine())!=null){
                        buildNotification(data);
                    }
                    bufferedReader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (upcomingList.size()==0){
            upcomingList.add(new Upcoming("No Upcoming Meeting Today"));
        }
        upcomingListView.getItems().addAll(upcomingList);
        System.out.println("getGeneralMeetings list "+upcomingList);
    }



    private void saveGeneralMeeting( String data){
        String date = "";
        try {
            JSONObject jsonObject = new JSONObject(data);
            int day = jsonObject.optInt("day");
            int month = jsonObject.optInt("month");
            int year = jsonObject.optInt("year");
            date = year+"-"+month+"-"+day;
        }catch (Exception e){
            e.printStackTrace();
        }
        File file = new File(folder);
        if(!file.exists()){
            file.mkdir();
        }
        try {
            String filename = folder+File.separator+"generalmeeting_"+date+".txt";
            System.out.println(filename);
            PrintWriter printWriter = new PrintWriter(new FileWriter(filename,true));
            printWriter.println(data);
            printWriter.close();

            Controller controller = new Controller();
            controller.launchController(stage);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private void showMessage(String msg) {
        Dialog<String> dialog = new Dialog<String>();
        dialog.setTitle("Message");
        ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        dialog.setContentText(msg);
        dialog.getDialogPane().getButtonTypes().add(type);
        dialog.showAndWait();
    }

    public void connectionLost(Throwable throwable) {
        System.out.println("Connection to MQTT broker lost! "+throwable);
        /*try {
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        switch (s) {
            case "nal_general":
                String json = "" + new String(mqttMessage.getPayload());
                buildNotification(json);
                buildPostNotification(json);
                saveGeneralMeeting(json);
                break;
            case "nal_holiday":
                String filepath = "" + new String(mqttMessage.getPayload());
                filepath = filepath.replace("\\","/");

                String filename = filepath.substring(filepath.lastIndexOf("/")+1);
                String cur_filename = this.filepath.substring(this.filepath.lastIndexOf("\\")+1);
                System.out.println("filename "+filename);
                System.out.println("cur_filename "+cur_filename);
                if(!cur_filename.equals(filename)){
                    download(filepath,filename);
                }

                break;
            case "nal_img":
                filepath = "" + new String(mqttMessage.getPayload());
                filepath = filepath.replace("\\","/");
                downloadImg(filepath,"nal_bg.jpg");
        }
    }

    private void buildGeneralNotification(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String time = jsonObject.optString("time");
            String title = jsonObject.optString("title");
            String message = jsonObject.optString("message") + " at " + time ;
            String animationType = jsonObject.optString("animationType");
            String notificationType = jsonObject.optString("notificationType");
            String date = jsonObject.optString("date");

            generalList.add(new General(title,message,date,time,notificationType,animationType));

            Service<Void> service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            //Background work
                            final CountDownLatch latch = new CountDownLatch(1);
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        //FX Stuff done here

                                        setTray(title, message, animationType, notificationType);
                                        tray.showAndWait();
                                        playSomeSound();
                                    } finally {
                                        latch.countDown();
                                    }
                                }
                            });
                            latch.await();
                            //Keep with the background work
                            return null;
                        }
                    };
                }
            };
            service.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void buildNotification(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String time = jsonObject.optString("time");
            String title = jsonObject.optString("title");
            String message = jsonObject.optString("message") + " at " + time ;
            String animationType = jsonObject.optString("animationType");
            String notificationType = jsonObject.optString("notificationType");
            String date = jsonObject.optString("date");

            upcomingList.add(new Upcoming(title,message,date,time,notificationType,animationType));

            Service<Void> service = new Service<Void>() {
                @Override
                protected Task<Void> createTask() {
                    return new Task<Void>() {
                        @Override
                        protected Void call() throws Exception {
                            //Background work
                            final CountDownLatch latch = new CountDownLatch(1);
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        //FX Stuff done here

                                        setTray(title, message, animationType, notificationType);
                                        tray.showAndWait();
                                        playSomeSound();
                                    } finally {
                                        latch.countDown();
                                    }
                                }
                            });
                            latch.await();
                            //Keep with the background work
                            return null;
                        }
                    };
                }
            };
            service.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void buildPostNotification(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            String time = jsonObject.optString("time");
            String date = jsonObject.optString("date");
            String title = jsonObject.optString("title");
            String message = jsonObject.optString("message")+" at "+time;
            String animationType = jsonObject.optString("animationType");
            String notificationType = jsonObject.optString("notificationType");
            int delay = jsonObject.optInt("delay");

            Date d = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(date + " " +time);
            int day = d.getDay();
            int month = d.getMonth();
            int year = d.getYear();
            int hour = d.getHours();
            int minute = d.getMinutes();


            Calendar calendar = Calendar.getInstance();

            calendar.set(Calendar.DATE,day);
            calendar.set(Calendar.MONTH,month-1);
            calendar.set(Calendar.HOUR,hour);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MINUTE,minute-delay);
            calendar.set(Calendar.YEAR,year);

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //FX Stuff done here
                                setTray(title, message + " at "+hour+":"+minute, animationType, notificationType);
                                tray.showAndWait();
                                playSomeSound();
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            Timer timer = new Timer();
            timer.schedule(timerTask,calendar.getTime());
            System.out.println(date);
            System.out.println(calendar.getTime());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        // not used in this example
    }

    public void connect() throws Exception{
        MqttClient client = new MqttClient(MQTT_URL, MqttClient.generateClientId());
        client.setCallback(this);
        client.connect();
        if(client.isConnected()) {
            System.out.println("Connected");
            client.subscribe(MQTT_TOPIC_GENERAL);
            client.subscribe(MQTT_TOPIC_HOLIDAY);
            client.subscribe(MQTT_TOPIC_IMG);
            System.out.println("subscribed");
        }
    }

    private void setTray(String title,String message,String animationType,String notificationType) {
        this.tray.setTitle(title);
        this.tray.setMessage(message);
        this.tray.setAnimationType(getAnimationType(animationType));
        final NotificationType selectedType = this.getNotificationType(notificationType);
        this.tray.setNotificationType(selectedType);
    }

    private AnimationType getAnimationType(String animationTypeString) {
        AnimationType animationType = null;
        switch (animationTypeString) {
            case "Slide": {
                animationType = AnimationType.SLIDE;
                break;
            }
            case "Fade": {
                animationType = AnimationType.FADE;
                break;
            }
            case "Popup": {
                animationType = AnimationType.POPUP;
                break;
            }
        }
        return animationType;
    }

    private NotificationType getNotificationType(String notificationType) {
        NotificationType notifyType = null;
        switch (notificationType) {
            case "Information": {
                notifyType = NotificationType.INFORMATION;
                break;
            }
            case "Notice": {
                notifyType = NotificationType.NOTICE;
                break;
            }
            case "Success": {
                notifyType = NotificationType.SUCCESS;
                break;
            }
            case "Warning": {
                notifyType = NotificationType.WARNING;
                break;
            }
            case "Error": {
                notifyType = NotificationType.ERROR;
                break;
            }
            case "Custom": {
                notifyType = NotificationType.CUSTOM;
                break;
            }
        }
        return notifyType;
    }
    private void playMedia(Media m){
        if (m != null){
            MediaPlayer mp = new MediaPlayer(m);
            mp.play();
        }
    }

    public void playSomeSound(){
        try{
            Media someSound = new Media(getClass().getResource("quite_impressed.mp3").toString());
            playMedia(someSound);
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public void download(String FILE_PATH,String FILE_NAME) {
        try {
            URL url = new URL(DOWNLOAD_URL+"?filename="+FILE_PATH);
            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
            httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
            BufferedInputStream in = new BufferedInputStream(httpcon.getInputStream());

            FileOutputStream fileOutputStream = new FileOutputStream(folder+File.separator+FILE_NAME);
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
            fileOutputStream.close();
            in.close();
            System.out.println(folder+File.separator+FILE_NAME+" downloaded");
            PrintWriter printWriter = new PrintWriter(new FileWriter(filepathtext));
            printWriter.println(folder+File.separator+FILE_NAME);
            printWriter.close();
            if(!filepath.equals(folder+File.separator+FILE_NAME)){
                filepath = folder+File.separator+FILE_NAME;
            }
            readExcel(filepath);
            System.out.println(filepathtext+" updated");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadImg(String FILE_PATH,String FILE_NAME) {
        try {
            URL url = new URL(DOWNLOAD_URL+"?filename="+FILE_PATH);
            HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
            httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
            BufferedInputStream in = new BufferedInputStream(httpcon.getInputStream());

            FileOutputStream fileOutputStream = new FileOutputStream(path+File.separator+FILE_NAME);
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
            fileOutputStream.close();
            in.close();
            System.out.println(path+File.separator+FILE_NAME+" downloaded");
            PrintWriter printWriter = new PrintWriter(new FileWriter(img_filepathtext));
            printWriter.println(path+File.separator+FILE_NAME);
            printWriter.close();
            if(!img_filepath.equals(path+File.separator+FILE_NAME)){
                img_filepath = folder+File.separator+FILE_NAME;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
