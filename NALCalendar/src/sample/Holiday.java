package sample;

public class Holiday {
    private String holidayName, holidayType;

    public Holiday(String holidayName,String holidayType){
        this.holidayName = holidayName;
        this.holidayType = holidayType;
    }
    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getHolidayType() {
        return holidayType;
    }

    public void setHolidayType(String holidayType) {
        this.holidayType = holidayType;
    }

    @Override
    public String toString() {
        return  holidayName ;
    }
}
