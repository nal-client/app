package sample;

import javafx.beans.property.SimpleStringProperty;

public class Upcoming {
    private String title;
    private String message;
    private String date;
    private String time;
    private String notificationType;
    private String animationType;

    public Upcoming(String message){
        this.message = message;
    }
    public Upcoming(String title,String message,String date, String time,String notificationType,String animationType){
        this.animationType = animationType;
        this.date = date;
        this.message = message;
        this.notificationType = notificationType;
        this.time = time;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getAnimationType() {
        return animationType;
    }

    public void setAnimationType(String animationType) {
        this.animationType = animationType;
    }

    @Override
    public String toString() {
        String str = getMessage();
        if(getTime() != null){
            str+=" at "+getTime();
        }
        return str;
    }
}
