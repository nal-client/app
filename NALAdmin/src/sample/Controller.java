package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.paho.client.mqttv3.*;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

public class Controller implements Initializable, MqttCallback {
    private static final String MQTT_URL = "tcp://broker.hivemq.com:1883";
    private static final String MQTT_TOPIC_GENERAL = "nal_general";
    private static final String MQTT_TOPIC_IMG = "nal_img";
    private static final String MQTT_TOPIC_HOLIDAY = "nal_holiday";
    @FXML
    private TextField txtTitle;
    @FXML
    private TextArea txtMessage;
    @FXML
    private ComboBox<String> cboNotificationType;
    @FXML
    private ComboBox<String> cboAnimations;
    @FXML
    private TextField dateSelected;
    @FXML
    private TextField timeSelected;
    @FXML
    private Button btnSend;
    @FXML
    private Button btnUpload;
    @FXML
    public TableView<Holiday> tbData;
    @FXML
    public TableColumn<Holiday,String> date;
    @FXML
    public TableColumn<Holiday,String> name;
    @FXML
    public TableColumn<Holiday,String> type;

    //String path=System.getProperty("user.home")+"\\nal_log.txt";
    //String path=System.getProperty("user.home")+"\\nalCalendar\\nal_log.txt";
    String folderpath="G:\\nalCalendar";
    String filepath="G:\\nalCalendar\\nal_log.txt";
    String img_filepath="G:\\nalCalendar\\nal_img.txt";
    String data="";

    MqttClient client;
    ArrayList<Holiday> holidayList;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.btnSend.setTooltip(new Tooltip("Send Notification"));
        this.btnUpload.setTooltip(new Tooltip("Upload Calendar"));
        this.cboNotificationType.getItems().addAll(new String[] { "Success", "Information", "Notice", "Warning", "Error"});
        this.cboAnimations.getItems().addAll(new String[] { "Slide", "Fade", "Popup" });
        this.cboNotificationType.getSelectionModel().selectFirst();
        this.cboAnimations.getSelectionModel().selectFirst();
        this.cboAnimations.setTooltip(new Tooltip("Select Animation type for notification"));
        this.cboNotificationType.setTooltip(new Tooltip("Select notification outlook"));
        this.txtTitle.setTooltip(new Tooltip("Enter title for notification"));
        this.txtMessage.setTooltip(new Tooltip("Enter body of message for notification"));
        dateSelected.setText(LocalDate.now().toString());
        timeSelected.setText(LocalTime.now().toString());
        dateSelected.setTooltip(new Tooltip("Select date to be notified on"));
        timeSelected.setTooltip(new Tooltip("Select time to be notified on"));

        try {
            client = new MqttClient(MQTT_URL, MqttClient.generateClientId());
            client.connect();
            client.setCallback(this);

        } catch (MqttException e) {
            e.printStackTrace();
        }
        holidayList = new ArrayList<>();
        checkFolderExist();
        checkFileExist();
        if(readFile()!=null){
            readExcel(readFile());
        }else{
            showMessage("Holiday List Empty. Please choose a file");
        }

        date.setCellValueFactory(new PropertyValueFactory<>("Date"));
        name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        type.setCellValueFactory(new PropertyValueFactory<>("Type"));


        //createServerSocket();
    }

    public void btnSend_onAction(ActionEvent actionEvent) throws Exception{
        String title=this.txtTitle.getText();
        String message=this.txtMessage.getText();
        String animationType=this.cboAnimations.getValue();
        String notificationType=this.cboNotificationType.getValue();
        String date= this.dateSelected.getText().toString();
        String time= this.timeSelected.getText().toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("message", message);
        jsonObject.put("animationType", animationType);
        jsonObject.put("notificationType", notificationType);
        jsonObject.put("date",date);
        jsonObject.put("time",time);
        String jsonString = jsonObject.toString();
        System.out.println(jsonString);
        if(client.isConnected()) {
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setPayload(jsonString.getBytes());
            client.publish(MQTT_TOPIC_GENERAL, mqttMessage);
            showMessage("Message Sent");
            clearFields();
        }
        else{
            System.out.println("Not Connected");
        }
    }

    private void clearFields(){
        txtMessage.setText("");
        txtTitle.setText("");
    }
    public void btnUpload_onAction(ActionEvent actionEvent) throws Exception {
        FileChooser fc =new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel File","*.xlsx")
        );
        File file = fc.showOpenDialog(null);

        if(file !=null) {
            PrintWriter p = new PrintWriter(new FileWriter(filepath));
            p.write("" + file.getAbsolutePath());
            p.close();
            if(client.isConnected()) {
                String path = file.getAbsolutePath();
                MqttMessage mqttMessage = new MqttMessage();
                mqttMessage.setPayload(path.getBytes());
                client.publish(MQTT_TOPIC_HOLIDAY, mqttMessage);
                showMessage("Message Sent");
                clearFields();
            }
            else{
                System.out.println("Not Connected");
            }
            showMessage("File Upload Successfully");

            readExcel(file.getAbsolutePath());
        }else{
            showMessage("Invalid File");
        }

    }

    public void btnImgUpload_onAction(ActionEvent actionEvent) throws Exception {
        FileChooser fc =new FileChooser();
        fc.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPEG","*.jpg")
        );
        File file = fc.showOpenDialog(null);

        if(file !=null) {
            PrintWriter p = new PrintWriter(new FileWriter(img_filepath));
            p.write("" + file.getAbsolutePath());
            p.close();
            if(client.isConnected()) {
                String path = file.getAbsolutePath();
                MqttMessage mqttMessage = new MqttMessage();
                mqttMessage.setPayload(path.getBytes());
                client.publish(MQTT_TOPIC_IMG, mqttMessage);
                showMessage("Message Sent");
                clearFields();
            }
            else{
                System.out.println("Not Connected");
            }
            showMessage("File Upload Successfully");
        }else{
            showMessage("Invalid File");
        }

    }

    private void upload(File file) {

    }

    private void checkFileExist() {
        File f=new File(filepath);
        if(!f.exists()){
            try {
                FileOutputStream fos=new FileOutputStream(filepath);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkFolderExist() {
        File f=new File(folderpath);
        if(!f.exists()){
            f.mkdir();
        }
    }

    private String readFile() {
        try {
            BufferedReader b=new BufferedReader(new FileReader(filepath));
            data =b.readLine();
            b.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return data;
    }
    private void showMessage(String msg) {
        Dialog<String> dialog = new Dialog<String>();
        dialog.setTitle("Message");
        ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        dialog.setContentText(msg);
        dialog.getDialogPane().getButtonTypes().add(type);
        dialog.showAndWait();
    }

    private void readExcel(String filePath) {
        try
        {
            FileInputStream file = new FileInputStream(new File(filePath));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext())
            {
                Row row = rowIterator.next();
                //For each row, iterate through all the columns
                Iterator<org.apache.poi.ss.usermodel.Cell> cellIterator = row.cellIterator();
                String names[] = new String[3];
                int i=0;
                while (cellIterator.hasNext())
                {
                    org.apache.poi.ss.usermodel.Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly

                    switch (cell.getCellType())
                    {
                        case org.apache.poi.ss.usermodel.Cell.CELL_TYPE_NUMERIC:
                            System.out.print(cell.getNumericCellValue() + "\t");
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.print(cell.getStringCellValue() + "\t");
                            names[i] = cell.getStringCellValue();
                            i++;
                            break;
                    }

                }
                Holiday holiday = new Holiday(names[0],names[1],names[2]);
                holidayList.add(holiday);
                System.out.println("");
            }
            file.close();
            ObservableList<Holiday> holidays = FXCollections.observableArrayList(
                    holidayList  //load data to table
            );
            tbData.setItems(holidays);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("Connection Lost due to "+cause);
        if(client!=null){
            try {
                client.connect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}