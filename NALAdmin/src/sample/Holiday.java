package sample;

import javafx.beans.property.SimpleStringProperty;

public class Holiday {
    private SimpleStringProperty date;
    private SimpleStringProperty name;
    private SimpleStringProperty type;

    public Holiday(String date, String name, String type) {
        this.date = new SimpleStringProperty(date);
        this.name = new SimpleStringProperty(name);
        this.type = new SimpleStringProperty(type);
    }

    public String getDate() {
        return date.get();
    }

    public SimpleStringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getType() {
        return type.get();
    }

    public SimpleStringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }
}
